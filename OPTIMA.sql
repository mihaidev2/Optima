-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 05 Apr 2017 la 15:44
-- Versiune server: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE `optima`;
USE `optima`;

--
-- Database: `optima`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `angajat`
--

CREATE TABLE `angajat` (
  `id` int(11) NOT NULL,
  `nume` varchar(16) NOT NULL,
  `prenume` varchar(16) NOT NULL,
  `cnp` bigint(11) NOT NULL,
  `buletin` varchar(64) NOT NULL,
  `email` varchar(32) NOT NULL,
  `telefon` int(16) NOT NULL,
  `functie` varchar(32) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `angajat`
--

INSERT INTO `angajat` (`id`, `nume`, `prenume`, `cnp`, `buletin`, `email`, `telefon`, `functie`, `updated_at`, `created_at`) VALUES
(4, 'Matei', 'Mihail', 1821109295918, 'PX 457122', 'mateimihai1@yahoo.com', 724567878, 'programator', '2017-04-05 10:41:02', '2017-03-25 07:18:28'),
(5, 'Raluca', 'Ionescu', 2830323584828, 'PX 567812', 'raluca.ionescu@gmail.com', 724456788, 'inginer', '2017-03-25 13:38:10', '2017-03-25 07:20:19'),
(6, 'Popescu', 'Gabriel', 1840929674518, 'PX 456728', 'popescu.gabriel@gmail.com', 723456789, 'relatii clienti', '2017-03-25 07:21:36', '2017-03-25 07:21:36'),
(7, 'Radu', 'Constantin', 1851209456818, 'PX 456318', 'radu.popescu@gmail.com', 723456789, 'consultant ING', '2017-03-27 03:47:47', '2017-03-25 07:26:04'),
(8, 'Bogdan', 'Andrei', 1820723565918, 'PX 145678', 'bogdan.andrei@gmail.com', 725680390, 'programator', '2017-03-27 16:28:36', '2017-03-25 07:27:55'),
(10, 'Radu', 'Arisanu', 1830917678918, 'PX 783456', 'radu.arisanu@siemens.ro', 724930130, 'inginer it', '2017-03-25 08:18:43', '2017-03-25 08:17:22'),
(14, 'Ionut', 'Popa', 1820722243418, 'PX 1235', 'ionut.popa@gmail.com', 724610448, 'programator', '2017-03-26 06:46:15', '2017-03-26 06:46:15'),
(15, 'Flavius', 'Aurel', 1781213674817, 'PX 3579', 'flavius.aurel@gmail.com', 724175384, 'profesor', '2017-03-26 07:34:59', '2017-03-26 07:34:59'),
(16, 'Popa', 'Stanciu', 1820815683818, 'PX 3679', 'popa.stanciu@gmail.com', 724567789, 'vanzator', '2017-03-26 07:36:26', '2017-03-26 07:36:26'),
(17, 'Raluca', 'Popescu', 1821109295918, 'PX 6827', 'raluca.popescu@gmail.com', 723567899, 'politist', '2017-04-03 13:15:03', '2017-03-26 07:39:05'),
(18, 'Stan', 'Dumitru', 1820321385718, 'PX 3845', 'stan.dumitru@gmail.com', 724583490, 'programator', '2017-03-27 03:49:18', '2017-03-27 03:49:18'),
(19, 'Radu', 'Ionut', 1821109238918, 'PX 1234', 'radu.ionut@gmail.com', 724567829, 'programator', '2017-03-27 11:22:47', '2017-03-27 11:22:47'),
(22, 'Popescu', 'Adrian', 1820918892618, 'PX 7826', 'popescu.adrian@gmail.com', 724657589, 'programator', '2017-03-27 11:26:05', '2017-03-27 11:26:05'),
(23, 'Marian', 'Costel', 1820730285918, 'PX 4679', 'marian.costel@gmail.com', 724567890, 'programator', '2017-03-27 11:40:12', '2017-03-27 11:40:12'),
(24, 'Matei', 'Mihail', 1821109295918, 'PX 6789', 'mateimihai1gm@gmail.com', 725678990, 'programator', '2017-04-03 13:13:59', '2017-04-03 10:56:54'),
(25, 'Nume test', 'Prenume test', 1821109295918, 'buletin', 'email@ya.ro', 724567890, 'functie', '2017-04-04 10:06:30', '2017-04-04 10:06:30'),
(26, 'Nume test 2', 'prenume test2', 1821109295918, 'buletin', 'email@yahoo.com', 756279310, 'funcite', '2017-04-04 10:07:55', '2017-04-04 10:07:55');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `apartament`
--

CREATE TABLE `apartament` (
  `id` int(11) NOT NULL,
  `id_asociatie` int(11) NOT NULL,
  `numar` int(11) NOT NULL,
  `blocul` varchar(16) NOT NULL,
  `scara` int(1) NOT NULL,
  `suprafata` int(4) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `apartament`
--

INSERT INTO `apartament` (`id`, `id_asociatie`, `numar`, `blocul`, `scara`, `suprafata`, `updated_at`, `created_at`) VALUES
(1, 1, 7, 'M35', 1, 60, '2017-04-05 10:42:18', '2017-03-29 09:49:15'),
(2, 2, 1, 'M34', 2, 45, '2017-04-02 10:22:54', '2017-03-31 08:12:48'),
(3, 3, 8, 'M34', 1, 80, '2017-04-01 06:42:42', '2017-03-31 08:13:08'),
(4, 5, 9, 'M34', 1, 78, '2017-04-01 06:50:08', '2017-03-31 08:13:37'),
(19, 11, 2, 'M39', 1, 65, '2017-04-01 07:03:02', '2017-04-01 06:47:44'),
(21, 2, 78, 'M34', 1, 100, '2017-04-05 04:44:14', '2017-04-05 04:42:50'),
(22, 1, 56, 'M37', 1, 100, '2017-04-05 04:43:42', '2017-04-05 04:43:42'),
(23, 12, 44, 'M38', 1, 100, '2017-04-05 05:27:57', '2017-04-05 04:46:03'),
(24, 3, 78, 'm45', 1, 120, '2017-04-05 10:42:25', '2017-04-05 10:41:59');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `asociatie`
--

CREATE TABLE `asociatie` (
  `id` int(11) NOT NULL,
  `id_angajat` int(11) NOT NULL,
  `nume` varchar(32) NOT NULL,
  `cif` varchar(11) NOT NULL,
  `adresa` varchar(255) NOT NULL,
  `reprezentant` varchar(64) NOT NULL,
  `responsabil` varchar(64) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `asociatie`
--

INSERT INTO `asociatie` (`id`, `id_angajat`, `nume`, `cif`, `adresa`, `reprezentant`, `responsabil`, `updated_at`, `created_at`) VALUES
(1, 4, 'Asociatie 1', 'RO6859662', 'Str.Republicii,nr9', 'Mihai Badea', 'Radu Florin', '2017-04-05 10:41:24', '2017-03-27 15:42:57'),
(2, 4, 'Asociatie 2', 'RO56374849', 'Str Ivan Anghelache, nr 10', 'Alin FLorea', 'Liviu Popescu', '2017-03-27 12:43:25', '2017-03-27 12:43:25'),
(3, 5, 'Asociatie 3', 'RO4356788', 'Str Stefan cel Mare, nr 8', 'Darius Marin', 'Bogdan Stan', '2017-03-28 05:00:39', '2017-03-27 15:57:53'),
(4, 6, 'Asociatie 4', 'RO5673849', 'Str. Florilor, nr 159', 'Gabi Bucse', 'Marius COsmin', '2017-03-27 15:58:54', '2017-03-27 15:58:54'),
(5, 7, 'Asociatie 5', 'RO567389', 'Str Brazilor, nr 6', 'Popescu Ionut', 'Bratu Voicu', '2017-03-27 16:37:34', '2017-03-27 16:37:34'),
(7, 16, 'Asociatie 6', 'RO7893789', 'Str Buzoieni, nr 10', 'Florin Popescu', 'Dragos Grigore', '2017-03-29 05:20:16', '2017-03-29 05:20:16'),
(11, 5, 'Asociatie 7', 'RO1122345', 'Str FLorilor, nr 8', 'Flavius Dumitru', 'Dumitru Cosmin', '2017-03-31 07:49:06', '2017-03-31 07:49:06'),
(12, 5, 'Asociatie 20', 'RO345667', 'Str Pangratii, nr 9', 'Vlad Muntean', 'Sorin Silviu', '2017-04-04 09:22:15', '2017-04-04 09:22:15'),
(13, 4, 'Asociatie 21', 'RO3456', 'Str Poiana Florilor, nr 70', 'Bogdan Draghici', 'Sorin Flaviu', '2017-04-05 04:47:06', '2017-04-04 09:25:20'),
(14, 6, 'Asociatie 22', 'RO7898', 'Str Ivan Anghelache, nr9', 'Gigi Dorin', 'Gabi Ionut', '2017-04-05 04:48:04', '2017-04-04 09:43:37'),
(15, 4, 'Nume', 'RO35', 'adresa', 'Reprez', 'Responsabil test', '2017-04-04 10:24:44', '2017-04-04 10:24:07'),
(16, 4, 'Nume test 2', 'RO4567', 'adresa test 2', 'reprez test 2', 'resp test 2', '2017-04-04 10:25:34', '2017-04-04 10:25:34'),
(17, 4, 'Nume test 3', 'RO34', '', 'Reprezentant ', 'Responsabil', '2017-04-04 10:40:39', '2017-04-04 10:40:39'),
(18, 5, 'Nume test 4', 'RO4567', 'adresa test 4', 'Reprezentant test 4', 'responsabil test 4', '2017-04-04 10:41:19', '2017-04-04 10:41:19'),
(19, 6, 'Nume test 5', 'RO5678', '', 'Reprez test 5', 'Responsabil test 5', '2017-04-04 10:42:19', '2017-04-04 10:42:19'),
(20, 4, 'Nume test 6', 'RO1234', '', 'repr', 'resp', '2017-04-04 10:44:25', '2017-04-04 10:44:25'),
(21, 10, 'Nume test 7', 'RO3456', '', 'Repr test 7', 'Resp test 7', '2017-04-04 12:38:48', '2017-04-04 12:38:00');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `locatar`
--

CREATE TABLE `locatar` (
  `id` int(11) NOT NULL,
  `id_apartament` int(11) NOT NULL,
  `prenume` varchar(16) NOT NULL,
  `nume` varchar(16) NOT NULL,
  `email` varchar(32) NOT NULL,
  `telefon` int(16) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `locatar`
--

INSERT INTO `locatar` (`id`, `id_apartament`, `prenume`, `nume`, `email`, `telefon`, `updated_at`, `created_at`) VALUES
(1, 3, 'Mihail', 'Matei', 'mateimihai1@yahoo.com', 724610448, '2017-04-05 10:28:58', '2017-04-02 09:27:46'),
(2, 1, 'Radu', 'Cosmin', 'radu.cosmin@gmail.com', 724356789, '2017-04-02 06:29:00', '2017-04-02 06:29:00'),
(3, 2, 'Popa', 'Ionescu', 'popai@gmail.com', 726589890, '2017-04-02 08:17:03', '2017-04-02 08:17:03'),
(5, 1, 'Vlad', 'Ionut', 'vlad.ionut@gmail.com', 724567890, '2017-04-02 12:38:51', '2017-04-02 12:38:51'),
(6, 2, 'Popescu', 'Adrian', 'popescu.adrian@gmail.com', 726458179, '2017-04-02 12:39:54', '2017-04-02 12:39:29'),
(8, 2, 'Popescu', 'Gabriel', 'popescu.gabi@gmail.com', 724610448, '2017-04-05 09:55:29', '2017-04-05 09:55:29'),
(9, 1, 'Radu', 'Arisanu', 'radu.arx@yahoo.com', 724930672, '2017-04-05 09:56:16', '2017-04-05 09:56:16'),
(10, 2, 'Radu', 'Popescu', 'rpopescu@yahoo.com', 724567345, '2017-04-05 10:43:12', '2017-04-05 10:43:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `angajat`
--
ALTER TABLE `angajat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apartament`
--
ALTER TABLE `apartament`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_asociatie` (`id_asociatie`);

--
-- Indexes for table `asociatie`
--
ALTER TABLE `asociatie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_angajat` (`id_angajat`);

--
-- Indexes for table `locatar`
--
ALTER TABLE `locatar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_apartament` (`id_apartament`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `angajat`
--
ALTER TABLE `angajat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `apartament`
--
ALTER TABLE `apartament`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `asociatie`
--
ALTER TABLE `asociatie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `locatar`
--
ALTER TABLE `locatar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
