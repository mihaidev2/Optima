<?php
require_once('../paths.php');
?>  
<html>
	<head>
		<title>Adauga asociatie</title>
		<link rel="stylesheet" href="{!! $siteurl !!}resources/assets/css/bootstrap.css" />
		<script type="text/javascript" src="{!! $siteurl !!}resources/assets/js/jquery-3.2.0.js"></script>
		<script type="text/javascript" src="{!! $siteurl !!}resources/assets/js/validations.js"></script>
		<style type="text/css">
		.header{
				margin-top:20px; 
				margin-bottom:5px;
				height:50px; 
				line-height:50px; 
				border-bottom:solid 1px silver;	
		}
		input[type=text] {
			width:200px;
		}
		input[type=submit] {
			color:green;
		}
		.btn_back { width:100%; border-bottom:solid 1px silver; margin-bottom:10px; }
		</style>
	</head>
	<body>
		<div class="wrapper">
			<div id="meniu">
				<span><a href="{{URL::to('angajat')}}">Angajat</a></span>
				<span><a href="{{URL::to('asociatie')}}">Asociatie</a></span>
				<span><a href="{{URL::to('apartament')}}">Apartament</a></span>
				<span><a href="{{URL::to('locatar')}}">Locatar</a></span>
			</div>
			<div class="header">
				<h1>Adauga asociatie</h1>
			</div>
			<div class="btn_back">
			<a href="{{URL::to('asociatie')}}">Inapoi</a>
			</div>
			<form action="{{URL::to('asociatie/save')}}" method="post" onsubmit="return validate_form_add_edit_asociatie();">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form">
				<table>
					<tr><td id="frm_errors" colspan="3"></td></tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td width="120">Nume *</td>
						<td width="200"><input type="text" name="nume" id="nume" /></td>
						<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;<-> Angajat *</td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td>CIF *</td>
						<td><input type="text" name="cif" id="cif" /></td>
						<td rowspan="10">
						&nbsp;&nbsp;&nbsp;<select name="id_angajat" id="id_angajat" size="5" style="width:200px;">
							@foreach ($angajati as $a)
							<option value="{!! $a->id !!}">{!! $a->nume . ' ' . $a->prenume !!}</option>
							@endforeach
						</select>
						</td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td>Adresa</td>
						<td><input type="text" name="adresa" id="adresa" /></td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td>Reprezentant *</td>
						<td><input type="text" name="reprezentant" id="reprezentant" /></td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td>Responsabil *</td>
						<td><input type="text" name="responsabil" id="responsabil" /></td>
					</tr>
					<tr><td height="4"></td></tr>
				</table>
				<div style="width:100%; border-top:solid 1px silver; padding-top:5px;">
					<input type="submit" name="btn_salveaza" value="Adauga" />
				</div>
			</div>
			</form>
		</div>
	</body>
</html>