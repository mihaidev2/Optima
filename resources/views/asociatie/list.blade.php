<?php
require_once('../paths.php');
?>
<html>
	<head>
		<title>Tabel asociatii</title>
		<link rel="stylesheet" href="{!! $siteurl !!}resources/assets/css/bootstrap.css" />
		<link rel="stylesheet" href="{!! $siteurl !!}resources/assets/css/jquery.dataTables.css">
		
		<script type="text/javascript" src="{!! $siteurl !!}resources/assets/js/jquery-3.2.0.js"></script>
		<script type="text/javascript" src="{!! $siteurl !!}resources/assets/js/jquery.dataTables.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$('table tbody tr').mouseover(function() {
					$(this).css({'background-color':'#999999', 'color':'white'});
				});
				$('table tbody tr').mouseout(function() {
					$(this).css({'background-color':'#FFFFFF', 'color':'black'});
				})
			});
		</script>
		
		<style type="text/css">
			.header{
				margin-top:20px; 
				margin-bottom:20px;
				height:25px; 
				line-height:25px; 	
			}
			.header { width:100%; }
			.header_first { float:left; border-bottom:solid 1px silver; width:50%; }
			.header_second { float:right; text-align:right; padding-right:10px; border-bottom:solid 1px silver; width:50%; margin-top:44px; }
			table thead th { background-color:#555; color: white; padding:5px 0px; border-left: solid 1px #FFFFFF; font-weight:normal; }
			table tbody td { border-right:solid 1px #555; border-bottom: solid 1px #555; padding: 2px 0px; }
			table td.first { border-left:solid 1px #555; }
		</style>
	</head>
	<body>
		<div class="wrapper">
			<div id="meniu">
				<span><a href="{{URL::to('angajat')}}">Angajat</a></span>
				<span><a href="{{URL::to('asociatie')}}">Asociatie</a></span>
				<span><a href="{{URL::to('apartament')}}">Apartament</a></span>
				<span><a href="{{URL::to('locatar')}}">Locatar</a></span>
			</div>
			<div class="header">
				<div class="header_first"><h1>Tabel asociatii</h1></div>
				<div class="header_second"><a href="{{URL::to('asociatie/new')}}">Adauga asociatie</a></div>
			</div>
			<div style="clear:both;"></div>
			<br/>
			<div style="height:450px; border:0; overflow:auto;">
			<table class="asociatii">
				<thead>
					<tr>
					<th width="45">ID</th>
					<th width="150">Angajat</th>
					<th width="150">Nume</th>
					<th width="150">Cif</th>
					<th width="150">Adresa</th>
					<th width="150">Reprezentant</th>
					<th width="150">Responsabil</th>
					<th width="150">Operatii</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($asociatii as $a)
					<tr>
						<td class="first">{!! $a->id !!}</td>
						<td>{!! $a->ang_nume . ' ' . $a->ang_prenume !!}</td>
						<td>{!! $a->nume !!}</td>
						<td>{!! $a->cif !!}</td>
						<td>{!! $a->adresa !!}</td>
						<td>{!! $a->reprezentant !!}</td>
						<td>{!! $a->responsabil !!}</td>
						<td>
							<a href="{{ URL::to('asociatie/edit', array($a->id)) }}">Editez</a>
							|
							<a href="javascript: var confirm=confirm('Doriti sa stergeti asociatia {!! $a->nume  !!}?'); 
								if(confirm) { window.location = '{{ URL::to('asociatie/delete', array($a->id)) }}'; }">Sterg</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			</div>
		</div>
		<script type="text/javascript">
			$(".asociatii").DataTable({
				select:true,
			});
		</script>
	</body>
</html>