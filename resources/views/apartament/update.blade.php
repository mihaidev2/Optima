<?php
require_once('../paths.php');
?>  
<html>
	<head>
		<title>Editeaza apartament</title>
		<link rel="stylesheet" href="{!! $siteurl !!}resources/assets/css/bootstrap.css" />
		<script type="text/javascript" src="{!! $siteurl !!}resources/assets/js/jquery-3.2.0.js"></script>
		<script type="text/javascript" src="{!! $siteurl !!}resources/assets/js/validations.js"></script>
		<style type="text/css">
		.header{
				margin-top:20px; 
				margin-bottom:5px;
				height:50px; 
				line-height:50px; 
				border-bottom:solid 1px silver;	
		}
		input[type=text] {
			width:200px;
		}
		input[type=submit] {
			color:green;
		}
		.btn_back { width:100%; border-bottom:solid 1px silver; margin-bottom:10px; }
		</style>
	</head>
	<body>
		<div class="wrapper">
			<div id="meniu">
				<span><a href="{{URL::to('angajat')}}">Angajat</a></span>
				<span><a href="{{URL::to('asociatie')}}">Asociatie</a></span>
				<span><a href="{{URL::to('apartament')}}">Apartament</a></span>
				<span><a href="{{URL::to('locatar')}}">Locatar</a></span>
			</div>
			<div class="header">
				<h1>Editeaza apartamentul nr {!! $apartament->numar !!}, din blocul {!! $apartament->blocul !!}</h1>
			</div>
			<div class="btn_back">
			<a href="{{URL::to('apartament')}}">Inapoi</a>
			</div>
			<form action="{{URL::to('apartament/updateInfo')}}" method="post" onsubmit="return validate_form_add_edit_apartament();">
			<div class="form">
				<table>
					<tr><td colspan="3" id="frm_errors"></td></tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td width="120">Numar *</td>
						<td width="200"><input type="text" name="nr" id="nr" value="{!! $apartament->numar !!}" onkeypress="return keyRestrict(event,'1234567890');" /></td>
						<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;<-> Asociatie *</td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td>Bloc *</td>
						<td><input type="text" name="bloc" id="bloc" value="{!! $apartament->blocul !!}" /></td>
						<td rowspan="10">
						&nbsp;&nbsp;&nbsp;<select name="id_asociatie" id="id_asociatie" size="5" style="width:200px;">
							@foreach ($asociatii as $a)
							<option value="{!! $a->id !!}" 
							@if( $a->id == $apartament->id_asociatie )
								selected="selected"
							@endif
							>{!! $a->nume !!}</option>
							@endforeach
						</select>
						</td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td>Scara *</td>
						<td><input type="text" name="scara" id="scara" value="{!! $apartament->scara !!}" onkeypress="return keyRestrict(event,'1234567890');" /></td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td>Suprafata *</td>
						<td><input type="text" name="suprafata" id="suprafata" value="{!! $apartament->suprafata !!}" onkeypress="return keyRestrict(event,'1234567890');" /></td>
					</tr>
					<tr><td height="4"></td></tr>
				</table>
				<div style="width:100%; border-top:solid 1px silver; padding-top:5px;">
					<input type="submit" name="btn_modifica" value="Modifica" />
					<a href="{{URL::to('apartament/new')}}"><input type="button" name="btn_adauga" value="Adauga" /></a>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id" value="{!! $apartament->id !!}" />
				</div>
			</div>
			</form>
		</div>
	</body>
</html>