<?php
require_once('../paths.php');
?>
<html>
	<head>
		<title>Tabel angajati</title>
		<link rel="stylesheet" href="{!! $siteurl !!}resources/assets/css/bootstrap.css" />
		<link rel="stylesheet" href="{!! $siteurl !!}resources/assets/css/jquery.dataTables.css">
		
		<script type="text/javascript" src="{!! $siteurl !!}resources/assets/js/jquery-3.2.0.js"></script>
		<script type="text/javascript" src="{!! $siteurl !!}resources/assets/js/jquery.dataTables.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$('table tbody tr').mouseover(function() {
					$(this).css({'background-color':'#999999', 'color':'white'});
				});
				$('table tbody tr').mouseout(function() {
					$(this).css({'background-color':'#FFFFFF', 'color':'#000000'});
				})
			});
		</script>
		
		<style type="text/css">
			.header{
				margin-top:20px; 
				margin-bottom:20px;
				height:25px; 
				line-height:25px; 	
			}
			.header { width:100%; }
			.header_first { float:left; border-bottom:solid 1px silver; width:50%; }
			.header_second { float:right; text-align:right; padding-right:10px; border-bottom:solid 1px silver; width:50%; margin-top:44px; }
			table thead th { background-color:#555; color: white; padding:5px 0px; border-left: solid 1px #FFFFFF; font-weight:normal; }
			table tbody td { border-right:solid 1px #555; border-bottom: solid 1px #555; padding: 2px 0px; }
			table td.first { border-left:solid 1px #555; }
		</style>
	</head>
	<body>
		<div class="wrapper">
			<div id="meniu">
				<span><a href="{{URL::to('angajat')}}">Angajat</a></span>
				<span><a href="{{URL::to('asociatie')}}">Asociatie</a></span>
				<span><a href="{{URL::to('apartament')}}">Apartament</a></span>
				<span><a href="{{URL::to('locatar')}}">Locatar</a></span>
			</div>
			<div class="header">
				<div class="header_first"><h1>Tabel angajati</h1></div>
				<div class="header_second"><a href="{{URL::to('angajat/new')}}">Adauga angajat</a></div>
			</div>
			<div style="clear:both;"></div>
			<br/>
			<div style="height:450px; border:0; overflow:auto;">
			<table class="angajati">
				<thead>
					<tr>
					<th width="45">ID</th>
					<th width="100">Nume</th>
					<th width="150">Prenume</th>
					<th width="150">Cnp</th>
					<th width="150">Buletin</th>
					<th width="250">Email</th>
					<th width="150">Telefon</th>
					<th width="150">Functie</th>
					<th width="150">Operatii</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($angajati as $a)
					<tr>
						<td class="first">{!! $a->id !!}</td>
						<td>{!! $a->nume !!}</td>
						<td>{!! $a->prenume !!}</td>
						<td>{!! $a->cnp !!}</td>
						<td>{!! $a->buletin !!}</td>
						<td>{!! $a->email !!}</td>
						<td>{!! $a->telefon !!}</td>
						<td>{!! $a->functie !!}</td>
						<td>
							<a href="{{ URL::to('angajat/edit', array($a->id)) }}">Editez</a>
							|
							<a href="javascript: var confirm=confirm('Doriti sa stergeti angajatul {!! $a->nume . ' ' . $a->prenume !!}?'); 
								if(confirm) { window.location = '{{ URL::to('angajat/delete', array($a->id)) }}'; }">Sterg</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			</div>
		</div>
		<script type="text/javascript">
			$(".angajati").DataTable({
				select:true,
			});
		</script>
	</body>
</html>