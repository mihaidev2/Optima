<?php
require_once('../paths.php');
?>
<html>
	<head>
		<title>Editare angajat</title>
		<link rel="stylesheet" href="{!! $siteurl !!}resources/assets/css/bootstrap.css" />
		<script type="text/javascript" src="{!! $siteurl !!}resources/assets/js/jquery-3.2.0.js"></script>
		<script type="text/javascript" src="{!! $siteurl !!}resources/assets/js/validations.js"></script>
		<style type="text/css">
		.header{
				margin-top:20px; 
				margin-bottom:5px;
				height:50px; 
				line-height:50px; 
				border-bottom:solid 1px silver;	
		}
		input[type=text] {
			width:200px;
		}
		input[type=submit] {
			color:green;
		}
		.btn_back { width:100%; border-bottom:solid 1px silver; margin-bottom:10px; }
		</style>
	</head>
	<body>
		<div class="wrapper">
			<div id="meniu">
				<span><a href="{{URL::to('angajat')}}">Angajat</a></span>
				<span><a href="{{URL::to('asociatie')}}">Asociatie</a></span>
				<span><a href="{{URL::to('apartament')}}">Apartament</a></span>
				<span><a href="{{URL::to('locatar')}}">Locatar</a></span>
			</div>
			<div class="header">
				<h1>Editare angajat {!! $angajat->prenume . ' ' . $angajat->nume !!}</h1>
			</div>
			<div class="btn_back">
			<a href="{{URL::to('angajat')}}">Inapoi</a>
			</div>
			<form action="{{URL::to('angajat/updateInfo')}}" method="post" onsubmit="return validate_form_add_edit_angajat();">
			<div class="form">
				<table>
					<tr>
						<td width="100">Nume *</td>
						<td width="200"><input type="text" name="nume" id="nume" value="{!! $angajat->nume !!}" /></td>
						<td width="400"><span id="tx_err_nume"></span></td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td width="100">Prenume *</td>
						<td width="200"><input type="text" name="prenume" id="prenume" value="{!! $angajat->prenume !!}" /></td>
						<td><span id="tx_err_prenume"></span></td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td width="100">CNP *</td>
						<td width="200"><input type="text" name="cnp" id="cnp" value="{!! $angajat->cnp !!}" onkeypress="return keyRestrict(event,'1234567890');" /></td>
						<td><span id="tx_err_cnp"></span></td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td width="100">Buletin *</td>
						<td width="200"><input type="text" name="buletin" id="buletin" value="{!! $angajat->buletin !!}" /></td>
						<td><span id="tx_err_buletin"></span></td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td width="100">Email *</td>
						<td width="200"><input type="text" name="email" id="email" value="{!! $angajat->email !!}" /></td>
						<td><span id="tx_err_email"></span></td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td width="100">Telefon *</td>
						<td width="200"><input type="text" name="telefon" id="telefon" value="{!! $angajat->telefon !!}" /></td>
						<td><span id="tx_err_telefon"></span></td>
					</tr>
					<tr><td height="4"></td></tr>
					<tr>
						<td width="100">Functie *</td>
						<td width="200"><input type="text" name="functie" id="functie" value="{!! $angajat->functie !!}" /></td>
						<td><span id="tx_err_functie"></span></td>
					</tr>
					<tr><td height="4"></td></tr>
				</table>
				<div style="width:100%; border-top:solid 1px silver; padding-top:5px;">
					<input type="submit" name="btn_salveaza" value="Modifica" />
					<a href="{{URL::to('angajat/new')}}"><input type="button" name="btn_salveaza" value="Adauga" /></a>
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<input type="hidden" name="id" value="{!! $angajat->id !!}"/>
				</div>
			</div>
			</form>
		</div>
	</body>
</html>