//
// Javascript document
//

// Useful:
function trim(str)
{
	return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

function validare_email(mail)
{
	var x = mail;
	var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (filter.test(x)) return true;
	else return false;
}

function validareCNP(s)
{
	var suma=0;
	if(s.length==13)
	{
		suma=parseInt(s.charAt(0))*2+parseInt(s.charAt(1))*7+parseInt(s.charAt(2))*9+parseInt(s.charAt(3))*1+parseInt(s.charAt(4))*4+parseInt(s.charAt(5))*6+parseInt(s.charAt(6))*3+parseInt(s.charAt(7))*5+parseInt(s.charAt(8))*8+parseInt(s.charAt(9))*2+parseInt(s.charAt(10))*7+parseInt(s.charAt(11))*9;
		suma=suma%11;
		if(suma==10)
		suma=1;
		if(suma==parseInt(s.charAt(12)))
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	else
	{
		return false;
	}
}

function keyRestrict(e, validchars) {
 var key='', keychar='';
 key = getKeyCode(e);
 if (key == null) return true;
 keychar = String.fromCharCode(key);
 keychar = keychar.toLowerCase();
 validchars = validchars.toLowerCase();
 if (validchars.indexOf(keychar) != -1)
  return true;
 if ( key==null || key==0 || key==8 || key==9 || key==13 || key==27 )
  return true;
 return false;
}

function getKeyCode(e)
{
 if (window.event)
    return window.event.keyCode;
 else if (e)
    return e.which;
 else
    return null;
}
// -- end useful


// Validations for formular add/edit angajat
function validate_form_add_edit_angajat() {
	
	var nume 	= trim($('#nume').val());
	var prenume = trim($('#prenume').val());
	var cnp 	= trim($('#cnp').val());
	var buletin = trim($('#buletin').val());
	var email 	= trim($('#email').val());
	var telefon = trim($('#telefon').val());
	var functie = trim($('#functie').val());
	
	var arr_errors = new Array();
	arr_errors['err_nume'] 		= 0;
	arr_errors['err_prenume'] 	= 0;
	arr_errors['err_cnp'] 		= 0;
	arr_errors['err_buletin'] 	= 0;
	arr_errors['err_email'] 	= 0;
	arr_errors['err_telefon'] 	= 0;
	arr_errors['err_functie'] 	= 0;
	
	var arr_tx_errors = new Array();
	arr_tx_errors['err_tx_nume'] 	= '';
	arr_tx_errors['err_tx_prenume'] = '';
	arr_tx_errors['err_tx_cnp'] 	= '';
	arr_tx_errors['err_tx_buletin'] = '';
	arr_tx_errors['err_tx_email'] 	= '';
	arr_tx_errors['err_tx_telefon'] = '';
	arr_tx_errors['err_tx_functie'] = '';
	
	var cnt_errors = 0;
	
	// Validate the form add/edit angajat
	// > field Nume
	if( nume.length == 0 ) {
		arr_tx_errors['err_tx_nume'] = 'Completati campul Nume.';
		arr_errors['err_nume'] = 1;
		cnt_errors++;
	} else if( nume.length < 2 ) {
		arr_tx_errors['err_tx_nume'] = 'Minim 2 caractere pentru campul Nume.';
		arr_errors['err_nume'] = 1;
		cnt_errors++;
	}
	
	// > field Prenume
	if( prenume.length == 0 ) {
		arr_tx_errors['err_tx_prenume'] = 'Completati campul Prenume.';
		arr_errors['err_prenume'] = 1;
		cnt_errors++;
	} else if( prenume.length < 2 ) {
		arr_tx_errors['err_tx_prenume'] = 'Minim 2 caractere pentru campul Prenume';
		arr_errors['err_prenume'] = 1;
		cnt_errors++;
	}
	
	// > field CNP
	if( cnp.length == 0 ) {
		arr_tx_errors['err_tx_cnp'] = 'Completati campul CNP.';
		arr_errors['err_cnp'] = 1;
		cnt_errors++;
	} else if( cnp.length != 13 ) {
		arr_tx_errors['err_tx_cnp'] = 'Campul CNP trebuie sa contina fix 13 caractere.';
		arr_errors['err_cnp'] = 1;
		cnt_errors++;
	} else if( validareCNP( cnp ) === false ) {
		arr_tx_errors['err_tx_cnp'] = 'CNP invalid.';
		arr_errors['err_cnp'] = 1;
		cnt_errors++;
	}
	
	// > field Buletin
	if( buletin.length == 0 ) {
		arr_tx_errors['err_tx_buletin'] = 'Completati campul Buletin.';
		arr_errors['err_buletin'] = 1;
		cnt_errors++;
	} else if( buletin.length < 5 ) {
		arr_tx_errors['err_tx_buletin'] = 'Minim 5 caractere pentru campul Buletin.';
		arr_errors['err_buletin'] = 1;
		cnt_errors++;
	}
	
	// > field Email
	if( email.length == 0 ) {
		arr_tx_errors['err_tx_email'] = 'Completati campul Email.';
		arr_errors['err_email'] = 1;
		cnt_errors++;
	} else if( email.length < 6 ) {
		arr_tx_errors['err_tx_email'] = 'Minim 6 caracatere pentru campul Email.';
		arr_errors['err_email'] = 1;
		cnt_errors++;
	} else if( validare_email(email) === false ) {
		arr_tx_errors['err_tx_email'] = 'Email invalid.';
		arr_errors['err_email'] = 1;
		cnt_errors++;
	}
	
	// > field Telefon
	if( telefon.length == 0 ) {
		arr_tx_errors['err_tx_telefon'] = 'Completati campul Telefon.';
		arr_errors['err_telefon'] = 1;
		cnt_errors++;
	} else if( telefon.length < 9 ) {
		arr_tx_errors['err_tx_telefon'] = 'Minim 9 caractere pentru campul Telefon.';
		arr_errors['err_telefon'] = 1;
		cnt_errors++;
	}
	
	// > field Functie
	if( functie.length == 0 ) {
		arr_tx_errors['err_tx_functie'] = 'Completati campul Functie.';
		arr_errors['err_functie'] = 1;
		cnt_errors++;
	} else if( functie.length < 2 ) {
		arr_tx_errors['err_tx_functie'] = 'Minim 2 caractere pentru campul Functie.';
		arr_errors['err_functie'] = 1;
		cnt_errors++;
	}
	
	if( cnt_errors > 0 ) {
		// > field Nume
		if( arr_errors['err_nume'] == 1 ) {
			$('span#tx_err_nume').html( arr_tx_errors['err_tx_nume'] ).css('color', '#ff4d94');
			$('input#nume').css({'background-color':'#ff4d94', 'border':'solid 1px silver', 'height':'24px'});
		} else {
			$('span#tx_err_nume').html( '' );
			$('input#nume').css('background-color', 'white');
		}
		
		// > field Prenume
		if( arr_errors['err_prenume'] == 1 ) {
			$('span#tx_err_prenume').html( arr_tx_errors['err_tx_prenume'] ).css('color', '#ff4d94');
			$('input#prenume').css({'background-color':'#ff4d94', 'border':'solid 1px silver', 'height':'24px'});
		} else {
			$('span#tx_err_prenume').html( '' );
			$('input#prenume').css('background-color', 'white');
		}
		
		// > field CNP
		if( arr_errors['err_cnp'] == 1 ) {
			$('span#tx_err_cnp').html( arr_tx_errors['err_tx_cnp'] ).css('color', '#ff4d94');
			$('input#cnp').css({'background-color':'#ff4d94', 'border':'solid 1px silver', 'height':'24px'});
		} else {
			$('span#tx_err_cnp').html( '' );
			$('input#cnp').css('background-color', 'white');
		}
		
		// > field Buletin
		if( arr_errors['err_buletin'] == 1 ) {
			$('span#tx_err_buletin').html( arr_tx_errors['err_tx_buletin'] ).css('color', '#ff4d94');
			$('input#buletin').css({'background-color':'#ff4d94', 'border':'solid 1px silver', 'height':'24px'});
		} else {
			$('span#tx_err_buletin').html( '' );
			$('input#buletin').css('background-color', 'white');
		}
		
		// > field Email
		if( arr_errors['err_email'] == 1 ) {
			$('span#tx_err_email').html( arr_tx_errors['err_tx_email'] ).css('color', '#ff4d94');
			$('input#email').css({'background-color':'#ff4d94', 'border':'solid 1px silver', 'height':'24px'});
		} else {
			$('span#tx_err_email').html( '' );
			$('input#email').css('background-color', 'white');
		}
		
		// > field Telefon
		if( arr_errors['err_telefon'] == 1 ) {
			$('span#tx_err_telefon').html( arr_tx_errors['err_tx_telefon'] ).css('color', '#ff4d94');
			$('input#telefon').css({'background-color':'#ff4d94', 'border':'solid 1px silver', 'height':'24px'});
		} else {
			$('span#tx_err_telefon').html( '' );
			$('input#telefon').css('background-color', 'white');
		}
		
		// > field Functie
		if( arr_errors['err_functie'] == 1 ) {
			$('span#tx_err_functie').html( arr_tx_errors['err_tx_functie'] ).css('color', '#ff4d94');
			$('input#functie').css({'background-color':'#ff4d94', 'border':'solid 1px silver', 'height':'24px'});
		} else {
			$('span#tx_err_functie').html( '' );
			$('input#functie').css('background-color', 'white');
		}
		
		return false;
	
	// if I don't have errors
	} else {
		
		$('span#tx_err_nume').html( '' );
		$('input#nume').css('background-color', 'white');
		$('span#tx_err_prenume').html( '' );
		$('input#prenume').css('background-color', 'white');
		$('span#tx_err_cnp').html( '' );
		$('input#cnp').css('background-color', 'white');
		$('span#tx_err_buletin').html( '' );
		$('input#buletin').css('background-color', 'white');
		$('span#tx_err_email').html( '' );
		$('input#email').css('background-color', 'white');
		$('span#tx_err_telefon').html( '' );
		$('input#telefon').css('background-color', 'white');
		$('span#tx_err_functie').html( '' );
		$('input#functie').css('background-color', 'white');
		
		return true;
		
	}
}


function validate_form_add_edit_asociatie() 
{
	//var id_angajat 		= parseInt($('#id_angajat').val());
	if($('#id_angajat').val() == null) { // 0 = no option selected
		var id_angajat = 0; 
	} else {
		var id_angajat = parseInt($('#id_angajat').val());
	}
	var nume 			= trim($('#nume').val());
	var cif 			= trim($('#cif').val());
	var reprezentant 	= trim($('#reprezentant').val());
	var responsabil 	= trim($('#responsabil').val());
	
	var arr_errors 					= new Array();
	arr_errors['err_id_angajat'] 	= 0;
	arr_errors['err_nume'] 			= 0;
	arr_errors['err_cif'] 			= 0;
	arr_errors['err_reprezentant'] 	= 0;
	arr_errors['err_responsabil'] 	= 0;
	
	var cnt_errors 	= 0;
	var tx_errors 	= '';
	
	// Validate the form add/edit asociatie
	// > Id angajat field
	if( parseInt(id_angajat) == 0 ) {
		//alert('ajuns');
		tx_errors += 'Selectati un angajat.<br/>';
		arr_errors['err_id_angajat'] = 1;
		cnt_errors++;
	}
	
	// > Nume field
	if( nume.length == 0 ) {
		tx_errors += 'Completati campul Nume.<br/>';
		arr_errors['err_nume'] = 1;
		cnt_errors++;
	} else if( nume.length < 2 ) {
		tx_errors += 'Cel putin 2 caractere pentru campul Nume.<br/>';
		arr_errors['err_nume'] = 1;
		cnt_errors++;
	}
	
	// > CIF field
	if( cif.length == 0 ) {
		tx_errors += 'Completati campul CIF.<br/>';
		arr_errors['err_cif'] = 1;
		cnt_errors++;
	} else if( cif.length < 3 ) {
		tx_errors += 'Minim 3 caractere pentru campul CIF.<br/>';
		arr_errors['err_cif'] = 1;
		cnt_errors++;
	}
	
	// > Reprezentant field
	if( reprezentant.length == 0 ) {
		tx_errors += 'Completati campul Reprezentant.<br/>';
		arr_errors['err_reprezentant'] = 1;
		cnt_errors++;
	} else if( reprezentant.length < 2 ) {
		tx_errors += 'Cel putin 2 caractere pentru campul Reprezentant.<br/>';
		arr_errors['err_reprezentant'] = 1;
		cnt_errors++;
	}
	
	// > Responsabil field
	if( responsabil.length == 0 ) {
		tx_errors += 'Completati campul Responsabil.<br/>';
		arr_errors['err_responsabil'] = 1;
		cnt_errors++;
	} else if( responsabil.length < 2 ) {
		tx_errors += 'Minim 2 caractere pentru campul Responsabil.<br/>';
		arr_errors['err_responsabil'] = 1;
		cnt_errors++;
	}
	
	// if I have errors
	if( cnt_errors > 0 ) {
		$('td#frm_errors').html( tx_errors ).css('color', '#ff4d94');
		
		// > Angajat field
		if( arr_errors['err_id_angajat'] == 1 ) {
			$('select#id_angajat').css('background-color', '#ff4d94');
		} else {
			$('select#id_angajat').css('background-color', 'white');
		}
		
		// > Nume field
		if( arr_errors['err_nume'] == 1 ) {
			$('input#nume').css('background-color', '#ff4d94');
		} else {
			$('input#nume').css('background-color', 'white');
		}
		
		// > CIF field
		if( arr_errors['err_cif'] == 1 ) {
			$('input#cif').css('background-color', '#ff4d94');
		} else {
			$('input#cif').css('background-color', 'white');
		}
		
		// > Reprezentant field
		if( arr_errors['err_reprezentant'] == 1 ) {
			$('input#reprezentant').css('background-color', '#ff4d94');
		} else {
			$('input#reprezentant').css('background-color', 'white');
		}
		
		// > Responsabil field
		if( arr_errors['err_responsabil'] == 1 ) {
			$('input#responsabil').css('background-color', '#ff4d94');
		} else {
			$('input#responsabil').css('background-color', 'white');
		}
		return false;
	}
	
	// if I don't have errors
	else {
		
		$('td#frm_errors').html( '' );
		$('select#id_angajat').css('background-color', 'white');
		$('input#nume').css('background-color', 'white');
		$('input#cif').css('background-color', 'white');
		$('input#reprezentant').css('background-color', 'white');
		$('input#responsabil').css('background-color', 'white');
		
		return true;
		
	}
	
}


function validate_form_add_edit_apartament() 
{
	if($('select#id_asociatie').val() == null) { // 0 = no option selected
		var id_asociatie = 0; 
	} else {
		var id_asociatie = parseInt($('select#id_asociatie').val());
	}
	//alert(id_asociatie);
	var numar 		= trim($('#nr').val());
	var bloc 		= trim($('#bloc').val());
	var scara 		= trim($('#scara').val());
	var suprafata 	= trim($('#suprafata').val());
	
	var arr_errors 					= new Array();
	arr_errors['err_id_asociatie'] 	= 0;
	arr_errors['err_numar'] 		= 0;
	arr_errors['err_bloc'] 			= 0;
	arr_errors['err_scara'] 		= 0;
	arr_errors['err_suprafata'] 	= 0;
	
	var tx_errors 	= '';
	var cnt_errors 	= 0;
	
	// Validate form add/edit apartament
	// > Asociatie field
	if( id_asociatie == 0 ) {
		tx_errors += 'Selectati o asociatie.<br/>';
		arr_errors['err_id_asociatie'] = 1;
		cnt_errors++;
	}
	
	// > Numar field
	if( numar.length == 0 ) {
		tx_errors += 'Completati campul numar.<br/>';
		arr_errors['err_numar'] = 1;
		cnt_errors++;
	}
	
	// > Bloc field
	if( bloc.length == 0 ) {
		tx_errors += 'Completati campul Bloc.<br/>';
		arr_errors['err_bloc'] = 1;
		cnt_errors++;
	} else if( bloc.length < 2 ) {
		tx_errors += 'Minim 2 caractere pentru campul Bloc.<br/>';
		arr_errors['err_bloc'] = 1;
		cnt_errors++;
	}
	
	// > Scara field
	if( scara.length == 0 ) {
		tx_errors += 'Completati campul Scara.<br/>';
		arr_errors['err_scara'] = 1;
		cnt_errors++;
	}
	
	// > Suprafata field
	if( suprafata.length == 0 ) {
		tx_errors += 'Completati campul Suprafata.<br/>';
		arr_errors['err_suprafata'] = 1;
		cnt_errors++;
	} else if( suprafata.length < 2 ) {
		tx_errors += 'Minim 2 caractere pentru campul Suprafata.<br/>';
		arr_errors['err_suprafata'] = 1;
		cnt_errors++;
	}
	
	// If I have errors
	if( cnt_errors > 0 ) {
		$('td#frm_errors').html( tx_errors ).css('color', '#ff4d94');
		// > Asociatie field
		if( arr_errors['err_id_asociatie'] == 1 ) {
			$('select#id_asociatie').css('background-color', '#ff4d94');
		} else {
			$('select#id_asociatie').css('background-color', 'white');
		}
		
		// > Numar field
		if( arr_errors['err_numar'] == 1 ) {
			$('input#nr').css('background-color', '#ff4d94');
		} else {
			$('input#nr').css('background-color', 'white');
		}
		
		// > Bloc field
		if( arr_errors['err_bloc'] == 1 ) {
			$('input#bloc').css('background-color', '#ff4d94');
		} else {
			$('input#bloc').css('background-color', 'white');
		}
		
		// > Scara field
		if( arr_errors['err_scara'] == 1 ) {
			$('input#scara').css('background-color', '#ff4d94');
		} else {
			$('input#scara').css('background-color', 'white');
		}
		
		// > Suprafata field
		if( arr_errors['err_suprafata'] == 1 ) {
			$('input#suprafata').css('background-color', '#ff4d94');
		} else {
			$('input#suprafata').css('background-color', 'white');
		}
		
		return false;
	}
	
	// If I don't have errors
	else {
		$('td#frm_errors').html( '' );
		$('select#id_asociatie').css('background-color', 'white');
		$('input#nr').css('background-color', 'white');
		$('input#bloc').css('background-color', 'white');
		$('input#scara').css('background-color', 'white');
		$('input#suprafata').css('background-color', 'white');
		
		return true;
	}
	
	
}


function validate_form_add_edit_locatar() 
{
	if($('select#id_apartament').val() == null) { // 0 = no option selected
		var id_apartament = 0; 
	} else {
		var id_apartament = parseInt($('select#id_apartament').val());
	}
	var prenume = trim($('input#prenume').val());
	var nume 	= trim($('input#nume').val());
	var email 	= trim($('input#email').val());
	var telefon = trim($('input#telefon').val());
	
	var arr_errors = new Array();
	arr_errors['err_id_apartament'] = 0;
	arr_errors['err_prenume'] 		= 0;
	arr_errors['err_nume'] 			= 0;
	arr_errors['err_email'] 		= 0;
	arr_errors['err_telefon'] 		= 0;
	
	var tx_errors 	= '';
	var cnt_errors 	= 0;
	
	// Validate the form add/edit locatar
	// > Apartament field
	if( id_apartament == 0 ) {
		tx_errors += 'Selectati un Apartament.<br/>';
		arr_errors['err_id_apartament'] = 1;
		cnt_errors++;
	}
	
	// > Prenume field
	if( prenume.length == 0 ) {
		tx_errors += 'Completati campul Prenume.<br/>';
		arr_errors['err_prenume'] = 1;
		cnt_errors++;
	} else if( prenume.length < 2 ) {
		tx_errors += 'Minim 2 caractere pentru campul Prenume.<br/>';
		arr_errors['err_prenume'] = 1;
		cnt_errors++;
	}
	
	// > Nume field
	if( nume.length == 0 ) {
		tx_errors += 'Completati campul Nume.<br/>';
		arr_errors['err_nume'] = 1;
		cnt_errors++;
	} else if( nume.length < 2 ) {
		tx_errors += 'Minim 2 caractere pentru campul Nume.<br/>';
		arr_errors['err_nume'] = 1;
		cnt_errors++;
	}
	
	// > Email field
	if( email.length == 0 ) {
		tx_errors += 'Completati campul Email.<br/>';
		arr_errors['err_email'] = 1;
		cnt_errors++;
	} else if( email.length < 6 ) {
		tx_errors += 'Minim 6 caractere pentru campul Email.<br/>';
		arr_errors['err_email'] = 1;
		cnt_errors++;
	} else if( validare_email( email ) == false ) {
		tx_errors += 'Email invalid.<br/>';
		arr_errors['err_email'] = 1;
		cnt_errors++;
	}
	
	// > Telefon field
	if( telefon.length == 0 ) {
		tx_errors += 'Completati campul Telefon.<br/>';
		arr_errors['err_telefon'] = 1;
		cnt_errors++;
	} else if(telefon.length < 9) {
		tx_errors += 'Minim 9 caractere pentru campul Telefon.<br/>';
		arr_errors['err_telefon'] = 1;
		cnt_errors++;
	}
	
	// If I have errors
	if(cnt_errors > 0) {
		$('td#frm_errors').html( tx_errors ).css('color', '#ff4d94');
		
		// > Apartament field
		if(arr_errors['err_id_apartament'] == 1) {
			$('select#id_apartament').css('background-color', '#ff4d94');
		} else {
			$('select#id_apartament').css('background-color', 'white');
		}
		
		// > Prenume field
		if(arr_errors['err_prenume'] == 1) {
			$('input#prenume').css('background-color', '#ff4d94');
		} else {
			$('input#prenume').css('background-color', 'white');
		}
		
		// > Nume field
		if(arr_errors['err_nume'] == 1) {
			$('input#nume').css('background-color', '#ff4d94');
		} else {
			$('input#nume').css('background-color', 'white');
		}
		
		// > Email field
		if(arr_errors['err_email'] == 1) {
			$('input#email').css('background-color', '#ff4d94');
		} else {
			$('input#email').css('background-color', 'white');
		}
		
		// > Telefon field
		if(arr_errors['err_telefon'] == 1) {
			$('input#telefon').css('background-color', '#ff4d94');
		} else {
			$('input#telefon').css('background-color', 'white');
		}
		
		return false;
	}
	
	// If I don't have errors
	else {
		$('td#frm_errors').html('');
		$('select#id_apartament').css('background-color', 'white');
		$('input#prenume').css('background-color', 'white');
		$('input#nume').css('background-color', 'white');
		$('input#email').css('background-color', 'white');
		$('input#telefon').css('background-color', 'white');
		
		return true;
	}
}