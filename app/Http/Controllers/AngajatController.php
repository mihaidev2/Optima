<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests\AngajatRequest;
use App\Http\Controllers\Redirect;
use App\Angajat;

class AngajatController extends BaseController {
		
	public function index() {
		$angajati = Angajat::all();
		return view('angajat.list', compact('angajati'));
		//return view('welcome');
	}
	
	public function newAngajat() {
		return view('angajat.insert');
	}
	
	public function save(AngajatRequest $request) {
		$a = new Angajat;
		$a->nume = $request->input('nume');
		$a->prenume = $request->input('prenume');
		$a->cnp = $request->input('cnp');
		$a->buletin = $request->input('buletin');
		$a->email = $request->input('email');
		$a->telefon = $request->input('telefon');
		$a->functie = $request->input('functie');
		$a->save();
		return back()->withInput();
	}
	
	public function edit($id) {
		$angajat = Angajat::where('id', $id)->first();
		return view('angajat.update', compact('angajat'));
	}
	
	public function updateInfo(AngajatRequest $request) {
		$a = new Angajat();
		$data = array('nume' => $request->input('nume'),
						'prenume' => $request->input('prenume'),
						'cnp' => $request->input('cnp'),
						'buletin' => $request->input('buletin'),
						'email' => $request->input('email'),
						'telefon' => $request->input('telefon'),
						'functie' => $request->input('functie'));
		$a->where('id', $request->input('id'))->update($data);
		return Redirect('angajat');
	}
	
	public function delete($id) {
		Angajat::where('id', $id)->delete();
		return back();
	}
	
}