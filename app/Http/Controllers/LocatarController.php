<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LocatarRequest;
use App\Http\Controllers\Redirect;
use App\Apartament;
use App\Locatar;

class LocatarController extends BaseController {

	public function index() {
		$locatari = DB::table('locatar')
					->join('apartament', 'locatar.id_apartament', '=', 'apartament.id')
					->join('asociatie', 'apartament.id_asociatie', '=', 'asociatie.id')
					->select('locatar.*', 'apartament.blocul', 'apartament.numar', 'asociatie.nume AS asoc_nume')
					->get();
		return view('locatar.list', compact('locatari'));
	}
	
	public function newLocatar() {
		$apartamente = Apartament::all();
		return view('locatar.insert', compact('apartamente'));
	}
	
	public function save(LocatarRequest $request) {
		$l = new Locatar;
		$l->id_apartament = $request->input('id_apartament');
		$l->prenume = $request->input('prenume');
		$l->nume = $request->input('nume');
		$l->email = $request->input('email');
		$l->telefon = $request->input('telefon');
		$l->save();
		return back()->withInput();
	}
	
	public function edit($id) {
		$locatar = Locatar::where('id', $id)->first();
		$apartamente = Apartament::all();
		return view('locatar.update', compact('locatar', 'apartamente'));
	}
	
	public function updateInfo(LocatarRequest $request) {
		$l = new Locatar;
		$data = array('id_apartament' => $request->input('id_apartament'),
						'prenume' => $request->input('prenume'),
						'nume' => $request->input('nume'),
						'email' => $request->input('email'),
						'telefon' => $request->input('telefon')
				);
		$l->where('id', $request->input('id'))->update($data);
		return Redirect('locatar');
	}
	
	public function delete($id) {
		Locatar::where('id', $id)->delete();
		return back();
	}

}