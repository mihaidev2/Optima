<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\AsociatieRequest;
use App\Http\Controllers\Redirect;
use App\Asociatie;
use App\Angajat;

class AsociatieController extends BaseController {
	
	public function index() {
		//return 'Bun venit in sectiunea Asociatie!';
		//$asociatii = Asociatie::all();
		$asociatii = DB::table('asociatie')
						->join('angajat', 'asociatie.id_angajat', '=', 'angajat.id')
						->select('asociatie.*', 'angajat.nume AS ang_nume', 'angajat.prenume AS ang_prenume')
						->get();
		return view('asociatie.list', compact('asociatii'));
	}
	
	public function newAsociatie() {
		$angajati = Angajat::all();
		return view('asociatie.insert', compact('angajati'));
	}
	
	public function save(AsociatieRequest $request) {
		$a = new Asociatie;
		$a->id_angajat = $request->input('id_angajat');
		$a->nume = $request->input('nume');
		$a->cif = $request->input('cif');
		$a->adresa = $request->input('adresa');
		$a->reprezentant = $request->input('reprezentant');
		//$a->responsabil
		$a->responsabil = $request->input('responsabil');
		$a->save();
		return back()->withInput();
	}
	
	public function edit($id) {
		$asociatie = Asociatie::where('id', $id)->first();
		$angajati = Angajat::all();
		return view('asociatie.update', compact('asociatie', 'angajati'));
	}
	
	public function updateInfo(AsociatieRequest $request) {
		$a = new Asociatie;
		$data = array('id_angajat' => $request->input('id_angajat'),
						'nume' => $request->input('nume'),
						'cif' => $request->input('cif'),
						'adresa' => $request->input('adresa'),
						'reprezentant' => $request->input('reprezentant'),
						'responsabil' => $request->input('responsabil'));
		$a->where('id', $request->input('id'))->update($data);
		return Redirect('asociatie');
	}
	
	public function delete($id) {
		Asociatie::where('id', $id)->delete();
		return back();
	}
	
}