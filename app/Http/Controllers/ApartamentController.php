<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ApartamentRequest;
use App\Http\Controllers\Redirect;
use App\Asociatie;
use App\Apartament;

class ApartamentController extends BaseController {
	
	public function index() {
		$apartamente = DB::table('apartament')
						->join('asociatie', 'apartament.id_asociatie', '=', 'asociatie.id')
						->select('apartament.*', 'asociatie.nume AS asoc_nume')
						->get();
		return view('apartament.list', compact('apartamente'));
	}
	
	public function newApartament() {
		$asociatii = Asociatie::all();
		return view('apartament.insert', compact('asociatii'));
	}
	
	public function save(ApartamentRequest $request) {
		$a = new Apartament;
		$a->id_asociatie = $request->input('id_asociatie');
		$a->numar = $request->input('nr');
		$a->blocul = $request->input('bloc');
		$a->scara = $request->input('scara');
		$a->suprafata = $request->input('suprafata');
		$a->save();
		//echo 'ajuns';
		return back()->withInput();
	}
	
	public function edit($id) {
		$apartament = Apartament::where('id', $id)->first();
		$asociatii = Asociatie::all();
		return view('apartament.update', compact('apartament', 'asociatii'));
	}
	
	public function updateInfo(ApartamentRequest $request) {
		$a = new Apartament;
		$data = array('id_asociatie' => $request->input('id_asociatie'),
						'numar' => $request->input('nr'),
						'blocul' => $request->input('bloc'),
						'scara' => $request->input('scara'),
						'suprafata' => $request->input('suprafata')
						);
		$a->where('id', $request->input('id'))->update($data);
		return Redirect('apartament');
	}
	
	public function delete($id) {
		Apartament::where('id', $id)->delete();
		return back();
	}
	
}