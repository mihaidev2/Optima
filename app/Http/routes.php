<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('angajat', 'AngajatController@index');
Route::get('angajat/new', 'AngajatController@newAngajat');
Route::post('angajat/save', 'AngajatController@save');
Route::get('angajat/edit/{id}', 'AngajatController@edit');
Route::get('angajat/delete/{id}', 'AngajatController@delete');
Route::post('angajat/updateInfo', 'AngajatController@updateInfo');

Route::get('asociatie', 'AsociatieController@index');
Route::get('asociatie/new', 'AsociatieController@newAsociatie');
Route::post('asociatie/save', 'AsociatieController@save');
Route::get('asociatie/edit/{id}', 'AsociatieController@edit');
Route::get('asociatie/delete/{id}', 'AsociatieController@delete');
Route::post('asociatie/updateInfo', 'AsociatieController@updateInfo');

Route::get('apartament', 'ApartamentController@index');
Route::get('apartament/new', 'ApartamentController@newApartament');
Route::post('apartament/save', 'ApartamentController@save');
Route::get('apartament/edit/{id}', 'ApartamentController@edit');
Route::get('apartament/delete/{id}', 'ApartamentController@delete');
Route::post('apartament/updateInfo', 'ApartamentController@updateInfo');

Route::get('locatar', 'LocatarController@index');
Route::get('locatar/new', 'LocatarController@newLocatar');
Route::post('locatar/save', 'LocatarController@save');
Route::get('locatar/edit/{id}', 'LocatarController@edit');
Route::get('locatar/delete/{id}', 'LocatarController@delete');
Route::post('locatar/updateInfo', 'LocatarController@updateInfo');

//Route::get('/', function () {
 //   return view('welcome');
//});
