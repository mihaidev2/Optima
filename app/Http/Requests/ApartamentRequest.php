<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class ApartamentRequest extends Request
{
	//
	//  Determin daca utilizatorul este autorizat sa fac acest request.
	//	@return bool
	//
	public function authorize() {
		return true;
	}
	
	//
	//	Obtin regulile de validare care se aplica la request.
	//	@return array
	//
	public function rules() {
		return [
			'id' => '',
			'id_asociatie' => 'required',
			'nr' => 'required',
			'bloc' => 'required',
			'scara' => 'required',
			'suprafata' => 'required',
		];
	}
}