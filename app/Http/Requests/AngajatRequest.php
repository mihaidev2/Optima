<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AngajatRequest extends Request
{
	//
	//  Determin daca utilizatorul este autorizat sa fac acest request.
	//	@return bool
	//
	public function authorize() {
		return true;
	}
	
	//
	//	Obtin regulile de validare care se aplica la request.
	//	@return array
	//
	public function rules() {
		return [
			'id' => '',
			'nume' => 'required',
			'prenume' => 'required',
			'cnp' => 'required',
			'buletin' => 'required',
			'email' => 'required',
			'telefon' => 'required',
			'functie' => 'required'
		];
	}
}